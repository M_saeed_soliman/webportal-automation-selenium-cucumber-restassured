package Bindings;


import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

import static io.restassured.RestAssured.*;

public class StepDefinitions {
    WebDriver driver;
    Actions action;
    WebDriverWait wait;
    JavascriptExecutor js;
    ChromeOptions option;


    private static RequestSpecification requestSpec;
    private static ResponseSpecification responseSpec;
    public static Response response;
    private static final Logger logger = LogManager.getLogger(StepDefinitions.class);



    public String accessToken;

    @Before
    public void initialization(){
        System.setProperty("webdriver.chrome.driver","D:\\chromedriver_win32\\chromedriver.exe");
        option = new ChromeOptions();
        option.addArguments("--window-size-1366,656");
        //option.addArguments("headless");
        option.setExperimentalOption("useAutomationExtension", false);
        //option.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));
        driver = new ChromeDriver(option);
        action = new Actions(driver);
        wait = new WebDriverWait(driver,30);
        //driver.manage().window().maximize();
        js = (JavascriptExecutor) driver;


        useRelaxedHTTPSValidation();
        accessToken =
                given().
                        header("Authorization","ReactClient:E7F76B83-3774-4CC5-80E0-C0860930D2D0").
                        header("Content-Type","application/x-www-form-urlencoded").
                        header("User-Agent","PostmanRuntime/7.26.8").
                        contentType("application/x-www-form-urlencoded").
                        formParam("grant_type","client_credentials").
                        formParam("scope","dashboardServerAPI.read dashboardServerAPI.write stockOrderManagmentAPI.read stockOrderManagmentAPI.write").
                        formParam("client_id","ReactClient").
                        formParam("client_secret","E7F76B83-3774-4CC5-80E0-C0860930D2D0").
                        log().all().
                        when().
                        post("https://localhost:5016/api/connect/token").
                        then().
                        log().all().extract().path("access_token");

    }

    @After
    public void TearDown(){

        driver.quit();
    }



    @Given("I Update RP as online")
    public void i_update_RP_as_online() {
        // Write code here that turns the phrase above into concrete actions
        requestSpec = new RequestSpecBuilder().setBaseUri("http://localhost:5029/v1").build();
        responseSpec = new ResponseSpecBuilder().expectStatusCode(200).build();

        String payload = "{\n" +
                "  \"online\": true,\n" +
                "  \"lastOnline\": \"2020-12-20T10:32:29.475Z\",\n" +
                "  \"name\": \"string\",\n" +
                "  \"pharmacistUsername\": \"test online\",\n" +
                "  \"missingRpLogs\": 0\n" +
                "}";
        given().
                spec(requestSpec).
                headers("Authorization","Bearer " + accessToken,"Content-Type",ContentType.JSON,"Accept","*/*").
                body(payload).
                log().all().
                when().
                post("/ResponsiblePharmacist").
                then().log().all().spec(responseSpec);


    }

    @When("I login to PIH")
    public void i_login_to_pih() {
        // Write code here that turns the phrase above into concrete actions
        driver.get("https://TheAddressIsMaskedForClientSecurity.cloud/");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.findElement(By.id("username")).sendKeys("reduser");
        driver.findElement(By.id("password")).sendKeys("Password1");
        driver.findElement(By.xpath("//input[@value='LOGIN TO YOUR HUB']")).click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    @Then("I should see RP as online")
    public void i_should_see_rp_as_online() {
        // Write code here that turns the phrase above into concrete actions
        String BackgroundColor = driver.findElement(By.xpath("(//span[text()='Red Store 5']/parent::div/parent::div)/preceding-sibling::div")).getCssValue("background-color");
        Assert.assertEquals("RP is not green in Tile","rgba(2, 174, 65, 1)",BackgroundColor);
        logger.info("RP is online in Tile");

        driver.findElement(By.xpath("(//div[text()='Responsible Pharmacist Status']/parent::span/parent::div/parent::div/parent::div/parent::div)/descendant::span[text()='View More']")).click();

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        String BackgroundColorinTable = driver.findElement(By.xpath("(//span[text()='test online']/parent::div)/preceding-sibling::div")).getCssValue("background-color");
        Assert.assertEquals("RP is not green in League Table","rgba(2, 174, 65, 1)",BackgroundColorinTable);
        logger.info("RP is online in League Table");


    }


    @Given("I Update RP as offline")
    public void i_update_RP_as_offline() {
        // Write code here that turns the phrase above into concrete actions
        requestSpec = new RequestSpecBuilder().setBaseUri("http://localhost:5029/v1").build();
        responseSpec = new ResponseSpecBuilder().expectStatusCode(200).build();

        String payload = "{\n" +
                "  \"online\": false,\n" +
                "  \"lastOnline\": \"2020-12-20T10:32:29.475Z\",\n" +
                "  \"name\": \"string\",\n" +
                "  \"pharmacistUsername\": \"test online\",\n" +
                "  \"missingRpLogs\": 0\n" +
                "}";
        given().
                spec(requestSpec).
                headers("Authorization","Bearer " + accessToken,"Content-Type",ContentType.JSON,"Accept","*/*").
                body(payload).
                log().all().
                when().
                post("/ResponsiblePharmacist").
                then().log().all().spec(responseSpec);


    }

    @Then("I should see RP as offline")
    public void i_should_see_rp_as_offline() {
        // Write code here that turns the phrase above into concrete actions
        String BackgroundColor = driver.findElement(By.xpath("(//span[text()='Red Store 5']/parent::div/parent::div)/preceding-sibling::div")).getCssValue("background-color");
        Assert.assertEquals("RP is not red in Tile","rgba(255, 6, 82, 1)",BackgroundColor);
        logger.info("RP is offline in Tile");

        driver.findElement(By.xpath("(//div[text()='Responsible Pharmacist Status']/parent::span/parent::div/parent::div/parent::div/parent::div)/descendant::span[text()='View More']")).click();

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        String BackgroundColorinTable = driver.findElement(By.xpath("(//span[text()='test online']/parent::div)/preceding-sibling::div")).getCssValue("background-color");
        Assert.assertEquals("RP is not red in League Table","rgba(255, 6, 82, 1)",BackgroundColorinTable);
        logger.info("RP is offline in League Table");


    }

    @Given("I send 20 Owings Total")
    public void i_send_owings_total() {
        // Write code here that turns the phrase above into concrete actions
        String payload = "{\n" +
                "  \"TimeStamp\": \"2020-12-02T13:45:00.000000Z\",\n" +
                "  \"StoreId\": \"LAMDR-69754\",\n" +
                "  \"Data\": \"{\",\n" +
                "  \"prescriptions\": \"{StandardPrescriptions:0,EpsPrescriptions:0,TotalPrescriptions:0,Items:0,EpsPercentage:00.0}\",\n" +
                "  \"Expiring claims\": \"NumberExpiringIn28Days:0,TotalCostExpiringIn28Days:0,NumberExpiringIn14Days:0,TotalCostExpiringIn14Days:0,NumberExpiringIn7Days:0,TotalCostExpiringIn7Days:0,NumberExpiringIn48Hours:0,TotalCostExpiringIn48Hours:0,NumberExpiringIn24Hours:0,TotalCostExpiringIn24Hours:0}\",\n" +
                "  \"total owings\": \"20\",\n" +
                "  \"uncollected prescriptions\": \"{UncollectedPrescriptionCountUnder42Days:0,UncollectedPrescriptionCountUnder56Days:0,UncollectedPrescriptionCountOver56Days:0}\"\n" +
                "  \"responsible pharmacist availability\": \"{Online:true,LastOnline:2020-11-30T10:50:51.6448771+00:00,Name:Monks rd,PharmacistUsername:Sign Out Event,MissingRpLogs:0}\",\n" +
                "  \"store availability\": \"{Available:true,LastAvailable:2020-11-24T12:31:19.74043+00:00}\",\n" +
                "}" + "\n" +
                "}";

    }

    @Given("I update Pharmacy as online")
    public void i_update_pharmacy_as_online() {

        requestSpec = new RequestSpecBuilder().setBaseUri("http://localhost:5029/v1").build();
        responseSpec = new ResponseSpecBuilder().expectStatusCode(200).build();

        String payload = "{\n" +
                "  \"available\": true,\n" +
                "  \"lastAvailable\": \"2020-12-27T13:39:19.185Z\"\n" +
                "}";

        given().
                spec(requestSpec).
                headers("Authorization","Bearer " + accessToken,"Content-Type",ContentType.JSON,"Accept","*/*").
                body(payload).
                log().all().
                when().
                post("/StoreAvailability").
                then().log().all().spec(responseSpec);

    }

    @Then("I should see Pharmacy as online")
    public void i_should_see_pharmacy_as_online() {

        driver.findElement(By.xpath("(//div[text()='Responsible Pharmacist Status']/parent::span/parent::div/parent::div/parent::div/parent::div)/descendant::span[text()='View More']")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        By ElementofPharmacyAvailability = By.xpath("(//div[text()='Red Store 5']/parent::div)/child::div[5]/span");
        String AvailabilityValue = driver.findElement(ElementofPharmacyAvailability).getText();
        Assert.assertEquals("Pharmacy is not online in League Table","ONLINE",AvailabilityValue);
        String AvailabilityColor = driver.findElement(ElementofPharmacyAvailability).getCssValue("color");
        Assert.assertEquals("Pharmacy Color is not green in League Table","rgba(94, 165, 0, 1)", AvailabilityColor);
        logger.info("Pharmacy is online in League Table");


    }

    @Given("I update Pharmacy as offline")
    public void i_update_pharmacy_as_offline() {

        requestSpec = new RequestSpecBuilder().setBaseUri("http://localhost:5029/v1").build();
        responseSpec = new ResponseSpecBuilder().expectStatusCode(200).build();

        String payload = "{\n" +
                "  \"available\": false,\n" +
                "  \"lastAvailable\": \"2020-12-27T13:39:19.185Z\"\n" +
                "}";

        given().
                spec(requestSpec).
                headers("Authorization","Bearer " + accessToken,"Content-Type",ContentType.JSON,"Accept","*/*").
                body(payload).
                log().all().
                when().
                post("/StoreAvailability").
                then().log().all().spec(responseSpec);

    }

    @Then("I should see Pharmacy as offline")
    public void i_should_see_pharmacy_as_offline() {

        driver.findElement(By.xpath("(//div[text()='Responsible Pharmacist Status']/parent::span/parent::div/parent::div/parent::div/parent::div)/descendant::span[text()='View More']")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        By ElementofPharmacyAvailability = By.xpath("(//span[text()='Store Availability']/parent::div)/following-sibling::span");
        String AvailabilityValue = driver.findElement(ElementofPharmacyAvailability).getText();
        Assert.assertEquals("Pharmacy is not offline in League Table","OFFLINE",AvailabilityValue);
        String AvailabilityColor = driver.findElement(ElementofPharmacyAvailability).getCssValue("color");
        Assert.assertEquals("Pharmacy Color is not red in League Table","rgba(255, 6, 82, 1)", AvailabilityColor);
        logger.info("Pharmacy is offline in League Table");
    }

    @Given("I Send {int} Claims with value £{int} Expiring in twenty-eight days")
    public void i_send_claims_expiring_in_twenty_eight_days(Integer NumberofExpiringClaims, Integer ClaimsValue) {
        requestSpec = new RequestSpecBuilder().setBaseUri("https://TheAddressIsMaskedForClientSecurity.cloud/v1").build();
        responseSpec = new ResponseSpecBuilder().expectStatusCode(200).build();

        String payload = "{\n" +
                "  \"TimeStamp\": \"2021-01-06T13:45:00.000000Z\",\n" +
                "  \"StoreId\": \"GRZLX-96034\",\n" +
                "  \"Data\": { \n" +
                "  \"Expiring claims\": \"{NumberExpiringIn28Days:25,TotalCostExpiringIn28Days:10000," +
                "NumberExpiringIn14Days:0,TotalCostExpiringIn14Days:0," +
                "NumberExpiringIn14Days:0,TotalCostExpiringIn14Days:0," +
                "NumberExpiringIn7Days:0,TotalCostExpiringIn7Days:0," +
                "NumberExpiringIn48Hours:0,TotalCostExpiringIn48Hours:0," +
                "NumberExpiringIn24Hours:0,TotalCostExpiringIn24Hours:0}\"" +
                    "}" +
                "}";

        given().
                spec(requestSpec).
                headers("Authorization","Bearer " + accessToken,
                        "Content-Type",ContentType.JSON,
                        "Accept","*/*",
                        "securitytoken","{7EA63BFA-01EC-4B7F-9D40-10F5A4C2E587}").
                body(payload).
                log().all().
                when().
                post("/messageimport").
                then().log().all().spec(responseSpec);
    }

    @Then("I should see Expiring claims Tile and League updated with {int} claims with value £{int} and in Black")
    public void i_should_see_expiring_claims_tile_and_league_updated_with_claims_and_in_black(Integer NumberofExpiringClaims, Integer ClaimsValue) {
        By ExpiringTileValue = By.xpath("((//div[text()='Expiring Claims']/parent::span/parent::div/parent::div/parent::div)/following-sibling::div)/descendant::div[4]/span[1]");
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(ExpiringTileValue)));
        String ExpiringTileActualValue = driver.findElement(ExpiringTileValue).getText();
        Assert.assertEquals("Actual Expiring Claims value in Tile is not £100","£100", ExpiringTileActualValue);
        logger.info("Actual Expiring Claims value in Tile is £100");
        By ExpiringTileClaimsCount = By.xpath("((//div[text()='Expiring Claims']/parent::span/parent::div/parent::div/parent::div)/following-sibling::div)/descendant::div[4]/span[2]");
        String ExpiringTileActualClaimsCount = driver.findElement(ExpiringTileClaimsCount).getText();
        Assert.assertEquals("Actual Count of Expiring Claims is not 25","25claims at risk of expiring",ExpiringTileActualClaimsCount);
        logger.info("Actual Count of Expiring Claims is 25");
        By ActualTimeFrameExpiringClaimsinTile = By.xpath("((//div[text()='Expiring Claims']/parent::span/parent::div/parent::div/parent::div)/following-sibling::div)/descendant::div[3]/div[2]");
        String ActualTimeFrameExpringClaimsinTile = driver.findElement(ActualTimeFrameExpiringClaimsinTile).getText();
        Assert.assertEquals("Time Frame of Expiring Claims is not 28 days", "in 28 days", ActualTimeFrameExpringClaimsinTile);
        logger.info("Time Frame of Expiring Claims is 28 days");
        driver.findElement(ExpiringTileValue).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        By LeagueTableTimeFrameHeader = By.xpath("//h1[text()='Financial Performance']/following::span[text()='Rank'][1]/parent::div/following-sibling::div[2]");
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(LeagueTableTimeFrameHeader)));
        String ActualLeagueTableHeaderTimeFrame = driver.findElement(LeagueTableTimeFrameHeader).getText();
        Assert.assertEquals("Actual LeagueTableHeader is not 28 days","Due to expire in 28 days", ActualLeagueTableHeaderTimeFrame);
        logger.info("Actual LeagueTableHeader is 28 days");

        By CountExpiringClaimsinLeagueTable = By.xpath("//div[text()='Red Store 5']/following-sibling::div[1]");
        String ActualClaimsCountinLeagueTable = driver.findElement(CountExpiringClaimsinLeagueTable).getText();
        Assert.assertEquals("Actual Count of Claims in League Table is not 25", "25", ActualClaimsCountinLeagueTable);
        logger.info("Actual Count of Claims in League Table is 25");

        By ValueExpiringClaimsinLeagueTable = By.xpath("//div[text()='Red Store 5']/following-sibling::div[2]");
        String ActualValueExpiringClaimsLeagueTable = driver.findElement(ValueExpiringClaimsinLeagueTable).getText();
        Assert.assertEquals("Actual Expiring Claims Value is not £100","£100",ActualValueExpiringClaimsLeagueTable);
        logger.info("Actual Expiring Claims Value is £100");


    }

}
