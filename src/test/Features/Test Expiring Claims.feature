Feature: Test Expiring Claim Tile and League Table
  #This scenario will first Send, through API, expiring claims in 28 days and then will check for it in PIH GUI
  #Then it will do the same for 14 days expiring, 7 days expiring, 48 hours expiring, 24 hours expiring
  #After each Update, it will login to PIH and check correct GUI

  Scenario: Test Expiring Claims in 28 days
    Given I Send 25 Claims with value £100 Expiring in twenty-eight days
    When I login to PIH
    Then I should see Expiring claims Tile and League updated with 25 claims with value £100 and in Black

#  Scenario: Test Expiring Claims in 14 days
#    Given I Send 35 Claims with value £200 Expiring in fourteen days
#    When I login to PIH
#    Then I should see Expiring claims Tile and League updated with 35 claims with value £200 and in Amber
#
#  Scenario: Test Expiring Claims in 7 days
#    Given I Send 45 Claims with value £300 Expiring in seven days
#    When I login to PIH
#    Then I should see Expiring claims Tile and League updated with 45 claims with value £300 and in Red
#
#  Scenario: Test Expiring Claims in 48 hours
#    Given I Send 55 Claims with value £400 Expiring in fourty-eight hours
#    When I login to PIH
#    Then I should see Expiring claims Tile and League updated with 55 claims with value £400 and in Red
#
#  Scenario: Test Expiring Claims in 24 hours
#    Given I Send 65 Claims with value £500 Expiring in twenty-eight days
#    When I login to PIH
#    Then I should see Expiring claims Tile and League updated with 65 claims with value £500 and in Red

