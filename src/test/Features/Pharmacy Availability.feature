Feature: Test Pharmacy Availiablity
  #in This feature, API will be used to set the pharmacy as online
  #Then a check will be done in GUI to check Pharmacy is actually online
  #The same Scenario will be repeated for Pharmacy offline

  Scenario: Test Pharmacy Online
    Given I update Pharmacy as online
    When I login to PIH
    Then I should see Pharmacy as online

  Scenario: Test Pharmacy Offline
    Given I update Pharmacy as offline
    When I login to PIH
    Then I should see Pharmacy as offline