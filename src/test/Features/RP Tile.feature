Feature: Test RP Tile
  #This scenario will first update RP as online through API and then check for it in PIH Tile and RP Table
  #Then it will repeat the same Scenario with RD offline

  Scenario: Test RP online
    Given I Update RP as online
    When I login to PIH
    Then I should see RP as online


  Scenario: Test RP offline
    Given I Update RP as offline
    When I login to PIH
    Then I should see RP as offline